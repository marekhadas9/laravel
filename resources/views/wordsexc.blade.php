@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$page_type}}</h1>
         @include('navbarmenu')
    </div>
    @include('abcpagination')

    <button id="modal" style="display:none" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Hint</button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Hint</h4>
                </div>
                <div class="modal-body">
                    <p class='hintetext'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    @if(!empty($words->count()))
    <div class="box">

        @if($route=='words')
        <div class="table-responsive" style='margin-top:20px'>
            <table class="table table-striped table-sm align-middle">
                <thead>
                    <tr>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                        <th>Hint</th>
                        <th>Check</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($words as $k=>$w)
                    <tr>
                        <td class="align-middle" val='{{$w->engname}}'>@if($k%2==0){{$w->engname}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->engname)}}'></input>@endif</td>
                        <td class="align-middle" val='{{$w->plname}}'>@if($k%2==1){{$w->plname}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->plname)}}'></input>@endif</td>
                        <td class="align-middle"><button class="btn btn-success hint" type="button">Display hint</button></td>
                        <td class="align-middle" val=''><button class="btn btn-info check" type="button">Check</button></td>
                    </tr>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        @endif

        @if($route=='phrasals' or $route=='idioms')
        <div class="table-responsive" style='margin-top:20px'>
            <table class="table table-striped table-sm align-middle">
                <thead>
                    <tr>

                        <th>English meaning</th>
                        <th>Polish meaning</th>
                        <th>Hint</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($words as $k=>$w)
                    <tr>
                        <td class="align-middle" val='{{$w->engname}}'>@if($k%2==0){{$w->engname}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->engname)}}'></input>@endif</td>
                        <td class="align-middle" val='{{$w->plname}}'>@if($k%2==1){{$w->plname}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->plname)}}'></input>@endif</td>
                        <td class="align-middle" val=''><button class="btn btn-success hint" type="button">Display hint</button></td>

                        @endforeach
                </tbody>
            </table>
        </div>
        @endif


        @if($route=='irregular')
        <div class="table-responsive" style='margin-top:20px'>
            <table class="table table-striped table-sm align-middle">
                <thead>
                    <tr>

                        <th>Present Tense</th>
                        <th>Simple Past</th>
                        <th>Past participate</th>
                        <th>Polish meaning</th>
                        <th>Hint</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($words as $k=>$w)
                    <tr>
                        <td class="align-middle" val='{{$w->eng_infinitive}}' nm="Present Tense">@if($k%2==0){{$w->eng_infinitive}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->eng_infinitive)}}'></input>@endif</td>
                        <td class="align-middle" val='{{$w->past2nd}}' nm="Simple Past">@if($k%2==1){{$w->past2nd}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->past2nd)}}'></input>@endif</td>
                        <td class="align-middle" val='{{$w->past3rd}}' nm="Past participate">@if($k%2==0){{$w->past3rd}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->past3rd)}}'></input>@endif</td>
                        <td class="align-middle" style="font-weight:bold" val='{{$w->plname}}' nm="Polish meaning">@if($k%2==1){{$w->plname}}@else <input type='text' value='{{App\Http\Controllers\Excercises::word_replace($w->plname)}}'></input>@endif</td>
                        <td class="align-middle"><button class="btn btn-success hint4" type="button">Display hint</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif



    </div>
    @else
    <div class="bs-callout bs-callout-danger" id="callout-inputgroup-text-input-only" style="border-left-color: #ce4844"><h4>There is nothing to display</h4> <p>Please change to other letter or display all results</p> </div>
    @endif
    {{ $words->links() }}
    <script type="text/javascript">$('.carousel').carousel({interval: false, keyboard: true})</script>
    @endsection
