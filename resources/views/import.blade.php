@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Import</h1>
        @include('navbarmenu')
    </div>


    <div class="alert alert-success" role="alert">
        <strong><span class="glyphicon glyphicon-comment"></span>
            Tip</strong> You can either update your database by uploading txt. file as well as 
        by filling up text area box with words you want to add 
    </div>

    @if (!empty($statusinsert))

    <div class="alert alert-success" style="vertical-align:middle;height:55px">
        <span style="vertical-align:middle"><strong>Success!</strong> following pronunciations were downloaded.</span>
        <button class="btn btn-info btn-sm float-right" data-toggle="collapse" href="#updatedprono" role="button" aria-expanded="false" aria-controls="errors">Show result</button>
    </div>

    <div class="collapse" id="updatedprono">
        <div class="card card-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>English meaning</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($statusinsert['success'] as $key => $noti)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$noti}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

    @if(!empty($data['not_inserted']))

    <div class="alert alert-danger" style="vertical-align:middle;height:55px">
        <span style="vertical-align:middle"><strong>Warning!</strong> {{count($data['not_inserted'])}} entry/ies were not inserted.</span>
        @if(!empty($data['not_inserted']))
        <button class="btn btn-danger btn-sm float-right" data-toggle="collapse" href="#errors" role="button" aria-expanded="false" aria-controls="errors">Show errors</button>
        @endif
    </div>
    <div class="collapse" id="errors">
        <div class="card card-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        @if(!empty($data['not_inserted'][0]['example']))
                        <tr>
                            <th>#</th>
                            <th>English meaning</th>
                            <th>Polish meaning</th>
                            <th>Example</th>
                            <th>Reason</th>
                        </tr>
                        @elseif(!empty($data['not_inserted'][0]['eng_infinitive']))
                        <tr>
                            <th>#</th>
                            <th>Present Tense</th>
                            <th>Simple Past</th>
                            <th>Past participate</th>
                            <th>Polish meaning</th>
                            <th>Reason</th>
                        </tr>
                        @else
                        <tr>
                            <th>#</th>
                            <th>English meaninge</th>
                            <th>Polish meaning</th>
                            <th>Reason</th>
                        </tr>
                        @endif
                    </thead>
                    <tbody>
                        @foreach ($data['not_inserted'] as $key => $noti)

                        @if(!empty($noti['example']))
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$noti['engname']}}</td>
                            <td>{{$noti['plname']}}</td>
                            <td>{{$noti['example']}}</td>
                            <td>{{$noti['reason']}}</td>
                        </tr>
                        @elseif(!empty($noti['eng_infinitive']))
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$noti['eng_infinitive']}}</td>
                            <td>{{$noti['past2nd']}}</td>
                            <td>{{$noti['past3rd']}}</td>
                            <td>{{$noti['plname']}}</td>
                            <td>{{$noti['reason']}}</td>
                        </tr>
                        @else
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$noti['engname']}}</td>
                            <td>{{$noti['plname']}}</td>
                            <td>{{$noti['reason']}}</td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

    @if (!empty($data)) 
    <p>

        @if (!empty($data['inserted']))
    <div class="alert alert-success" style="vertical-align:middle;height:55px">
        <span style="vertical-align:middle"><strong>Warning!</strong>{{count($data['inserted'])}} entries were successfully inserted.</span>
        @if (!empty($data['not_inserted']))

        <button class="btn btn-success btn-sm float-right" data-toggle="collapse" href="#success" role="button" aria-expanded="false" aria-controls="success">Show results</button>

        @endif
    </div>
    @endif
</p>
@endif

@if (!empty($data['inserted']))

<div class="collapse" id="success">
    <div class="card card-body">

        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    @if(!empty($data['not_inserted'][0]['example']))
                    <tr>
                        <th>#</th>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                        <th>Example</th>
                        <th>Reason</th>
                    </tr>
                    @elseif(!empty($data['not_inserted'][0]['eng_infinitive']))
                    <tr>
                        <th>#</th>
                        <th>Present Tense</th>
                        <th>Simple Past</th>
                        <th>Past participate</th>
                        <th>Polish meaning</th>
                        <th>Reason</th>
                    </tr>
                    @else
                    <tr>
                        <th>#</th>
                        <th>English meaninge</th>
                        <th>Polish meaning</th>
                        <th>Reason</th>
                    </tr>
                    @endif
                </thead>
                <tbody>
                    @foreach ($data['inserted'] as $key => $noti)

                    @if(!empty($noti['example']))
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$noti['engname']}}</td>
                        <td>{{$noti['plname']}}</td>
                        <td>{{$noti['example']}}</td>
                        <td>{{$noti['reason']}}</td>
                    </tr>
                    @elseif(!empty($noti['eng_infinitive']))
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$noti['eng_infinitive']}}</td>
                        <td>{{$noti['past2nd']}}</td>
                        <td>{{$noti['past3rd']}}</td>
                        <td>{{$noti['plname']}}</td>
                        <td>{{$noti['reason']}}</td>
                    </tr>
                    @else
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$noti['engname']}}</td>
                        <td>{{$noti['plname']}}</td>
                        <td>{{$noti['reason']}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
<div class="box">
    <form enctype="multipart/form-data" action="{{ route('importtfile') }}" method="POST">
        <div class="bs-callout bs-callout-info" id="callout-pagination-label" style="border-left-color: #aa6708"> 
            <h4>Update from external text file.</h4> 
            <p>You can update your database by using external text file.<br> To do so, your file must follow specific pattern specified below.</p> 
            <p>Words<br>
                eng@pl$<br><br>
                Idioms<br>
                eng@pl@example$<br><br>
                Phrasal verbs<br>
                eng@pl@example$<br><br>
                Irregular verbs<br>
                Present Tense@Simple Past@Past participate@Polish meaning$<br>

            </p>
        </div>
        <div class="form-group">

            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="1" checked="">
                            <label class="form-check-label" for="gridRadios1">
                                Words
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="2">
                            <label class="form-check-label" for="gridRadios2">
                                Idioms
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="3">
                            <label class="form-check-label" for="gridRadios3">
                                Phrasal verbs
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios4" value="4">
                            <label class="form-check-label" for="gridRadios4">
                                Irregular verbs
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>
            @csrf
            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="fileimpo">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Upload</button>
    </form>
</div>
<div class="box">
    <div class="bs-callout bs-callout-info" id="callout-pagination-label"> 
        <h4>Update by typing.</h4> 
        <p>You can also update your database by using text area bellow, to do so please follow pattern.</p> 
        <p>Words<br>
            eng@pl$<br><br>
            Idioms<br>
            eng@pl@example$<br><br>
            Phrasal verbs<br>
            eng@pl@example$<br><br>
            Irregular verbs<br>
            Present Tense@Simple Past@Past participate@Polish meaning$<br>

        </p>
    </div>
    <form action="{{ route('importtextarea') }}" method="POST" accept-charset="utf-8">
        <fieldset class="form-group">
            <div class="row">
                <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gridRadiosTextarea" id="gridRadiosTextarea1" value="1" checked="">
                        <label class="form-check-label" for="gridRadiosTextarea1">
                            Words
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gridRadiosTextarea" id="gridRadiosTextarea2" value="2">
                        <label class="form-check-label" for="gridRadiosTextarea2">
                            Idioms
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <input class="form-check-input" type="radio" name="gridRadiosTextarea" id="gridRadiosTextarea3" value="3">
                        <label class="form-check-label" for="gridRadiosTextarea3">
                            Phrasal verbs
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <input class="form-check-input" type="radio" name="gridRadiosTextarea" id="gridRadiosTextarea4" value="4">
                        <label class="form-check-label" for="gridRadiosTextarea4">
                            Irregular verbs
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Words</label>
            <textarea class="form-control" name="textareaimport" rows="16"></textarea>
            @csrf
        </div>
        <button type="submit" class="btn btn-primary mb-2">Save</button>
    </form>
</div>
<div class="box">
    <div class="bs-callout bs-callout-info" id="callout-pagination-label"> 
        <h4>Update text-to-speach database.</h4> 
        <p>By clicking button bellow you can update database of word pronunciation which is available above each new word you are learning</p> 

        </p>
    </div>

    <form action="{{ route('importpronunciation') }}" method="POST" accept-charset="utf-8">
        <button type="submit" class="btn btn-primary mb-2">Update pronunciation</button>
        @csrf
    </form>
</div>
</main>


@endsection
