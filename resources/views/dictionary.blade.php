@extends('layouts.app')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Dictionary</h1>
        @include('navbarmenu')
    </div>
    @include('abcpagination')
    <div class="box">
        @if(!empty($dictionary->count()))
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                        <th>Pronunciation</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($dictionary as $word)
                    <tr>
                        <td class="align-middle">{{$word->id}}</td>
                        <td class="align-middle">{{$word->engname}}</td>
                        <td class="align-middle">{{$word->plname}}</td>
                        <td class="align-middle">
                            @if(!empty($word->pronunciation))
                            <video style="height:60px;width:300px;" controls="controls" preload="auto">
                                <source src="{{url('/voices')}}/{{$word->pronunciation}}" type="video/mp4">
                            </video>
                            @else: 
                            - 
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="bs-callout bs-callout-info" id="callout-pagination-label" style="border-left-color: #aa6708"> 
            <h4>There is nothing to display here.</h4> 
            <p>Please go to <a href="{{ route('dictionary') }}"><b> main</b></a> dictionary <b>or select another letter</p> 
        </div>
        @endif
        {{ $dictionary->links() }}
    </div>
</main>


@endsection
