<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Proficient English 2.0</title>

        <script src="{{ asset('js/jquery.js') }}?ver={{time()}}" type="text/javascript"></script>
        
        
        <script src="{{ asset('js/moment.min.js') }}?ver={{time()}}" type="text/javascript"></script>
        <script src="{{ asset('js/fullcalendar.js') }}?ver={{time()}}" type="text/javascript"></script>
        
        <script src="{{ asset('js/jquery-ui.js') }}?ver={{time()}}" type="text/javascript"></script>
        <script src="{{ asset('js/bootstrap.js') }}?ver={{time()}}" type="text/javascript"></script>

        <link href="{{ asset('css/bootstrap.css') }}?ver={{time()}}" rel="stylesheet" type="text/css">
        <link rel='stylesheet' href='{{ asset('css/fullcalendar.css') }}?ver={{time()}}' />
        @if (Auth::check())

        <link href="{{ asset('css/dashboard.css') }}?ver={{time()}}" rel="stylesheet" type="text/css">
        <script src="{{ asset('js/app.js') }}?ver={{time()}}" type="text/javascript"></script>
        @endif
        <link href="{{ asset('css/jquery-ui.css') }}?ver={{time()}}" rel="stylesheet" type="text/css">


    </head>
    <body>
        @guest
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                @yield('content')
            </main>
        </div>
        @else
        <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ config('app.name') }} - {{ Auth::user()->name }}</a>
            <input class="form-control form-control-dark w-100" id="dashboardsSuggest" type="text" placeholder="Search" aria-label="Search">

            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">Sign out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Main</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                                <span data-feather="home"></span>
                            </a>
                        </h6>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == '' or Route::currentRouteName() == 'home') active @endif" href="{{ route('home') }}">
                                    <span data-feather="home"></span>
                                    Dashboard 
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'dictionary') active @endif" href="{{ route('dictionary') }}">
                                    <span data-feather="file"></span>
                                    Dictionary
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link @if(Route::currentRouteName() == 'import') active @endif" href="{{ route('import') }}">
                                    <span data-feather="layers"></span>
                                    Import
                                </a>
                            </li>

                        </ul>

                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Learn</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                                <span data-feather="book-open"></span>
                            </a>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'words') active @endif" href="{{ url('learn/words') }}">
                                    <span data-feather="file-text"></span>
                                    Words
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'phrasal') active @endif" href="{{ url('learn/phrasal') }}">
                                    <span data-feather="file-text"></span>
                                    Phrasal verbs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'idioms') active @endif" href="{{ url('learn/idioms') }}">
                                    <span data-feather="file-text"></span>
                                    Idioms
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'iregular') active @endif" href="{{ url('learn/iregular') }}">
                                    <span data-feather="file-text"></span>
                                    Irregular verbs
                                </a>
                            </li>
                        </ul>
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Excercises</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                                <span data-feather="clipboard"></span>
                            </a>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'excword') active @endif" href="{{ url('excercises/words') }}">
                                    <span data-feather="edit"></span>
                                    Words
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'phrasalexc') active @endif" href="{{ url('excercises/phrasal') }}">
                                    <span data-feather="edit"></span>
                                    Phrasal verbs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'idiomsexc') active @endif" href="{{ url('excercises/idioms') }}">
                                    <span data-feather="edit"></span>
                                    Idioms
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'iregularexc') active @endif" href="{{ url('excercises/iregular') }}">
                                    <span data-feather="edit"></span>
                                    Irregular verbs
                                </a>
                            </li>
                        </ul>
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Your progress</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                                <span data-feather="database"></span>
                            </a>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'wordprogress') active @endif" href="{{ url('progress/words') }}">
                                    <span data-feather="eye"></span>
                                    Words
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'phrasalprogress') active @endif" href="{{ url('progress/phrasal') }}">
                                    <span data-feather="eye"></span>
                                    Phrasal verbs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'idiomsprogress') active @endif" href="{{ url('progress/idioms') }}">
                                    <span data-feather="eye"></span>
                                    Idioms
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'iregularprogress') active @endif" href="{{ url('progress/iregular') }}">
                                    <span data-feather="eye"></span>
                                    Irregular verbs
                                </a>
                            </li>
                        </ul>
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Miscellaneous</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                                <span data-feather="grid"></span>
                            </a>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link @if(Route::currentRouteName() == 'tenses') active @endif" href="{{ url('miscellaneous/tenses') }}">
                                    <span data-feather="file-text"></span>
                                    Tenses
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                @yield('content')

            </div>
        </div>

        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
        <script>
feather.replace()
        </script>

        @endguest
    </body>
</html>
