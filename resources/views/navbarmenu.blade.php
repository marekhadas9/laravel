<div class="btn-toolbar mb-2 mb-md-0">
    @if(strpos(url()->current(),'learn')!==false)
    <div class="btn-group mr-2">
        <button class="btn btn-sm btn-outline-secondary" onclick="window.print()">Print</button>
        <button class="btn btn-sm btn-outline-secondary export" page="{{ collect(request()->segments())->last() }}">Export</button>
    </div>
    @endif
    <button class="btn btn-sm btn-outline-secondary" onclick="app.toggleFullScreen()">
        Full Screen
    </button>
</div>
