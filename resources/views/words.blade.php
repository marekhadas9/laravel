@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$page_type}}</h1>
         @include('navbarmenu')
    </div>
    @include('abcpagination')

    @if(!empty($words->count()))
    <div class="box" style="background:#272B30">

        <div id="carouselContent" class="carousel slide" data-ride="carousel" style="color:white">
            <ol class="carousel-indicators">
                @foreach($words as $k => $v)
                <li class="word_{{$v->id}}" data-target="#carouselContent" data-slide-to="{{$k}}" 
                    @if($k == 0)class="active" 
                    @endif
                    ></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">


                @foreach($words as $k => $v)
                <div class="word_{{$v->id}} carousel-item  

                     @if($k == 0)
                     active 
                     @endif 

                     text-center p-4">

                    <h1>
                        @if(empty($v->engname)) 
                        {{$v->eng_infinitive}} - {{$v->past2nd}} - {{$v->past3rd}}
                        @else
                        {{$v->engname}}
                        @endif    
                    </h1>
                    <h3 class="word" style="color:gold">{{$v->plname}}</h3>
                    @if(!empty($v->example))<h5 style="margin-bottom:30px">{{$v->example}}</h5> @endif
                    @if(!empty($v->pronunciation))

                    <video style="height:60px;width:300px;margin-bottom:20px" controls="controls" preload="auto">
                        <source src="{{url('/voices')}}/{{$v->pronunciation}}" type="video/mp4">
                    </video>
                    @elseif(!empty($v->pron_infinitive) or !empty($v->pron_2nd) or !empty($v->pron_3rd))
                        @if(!empty($v->pron_infinitive))
                        <video style="height:60px;width:300px;margin-bottom:20px" controls="controls" preload="auto">
                            <source src="{{url('/voices')}}/{{$v->pron_infinitive}}" type="video/mp4">
                        </video>
                        @endif
                        @if(!empty($v->pron_2nd))
                        <video style="height:60px;width:300px;margin-bottom:20px" controls="controls" preload="auto">
                            <source src="{{url('/voices')}}/{{$v->pron_2nd}}" type="video/mp4">
                        </video>
                        @endif
                        @if(!empty($v->pron_3rd))
                        <video style="height:60px;width:300px;margin-bottom:20px" controls="controls" preload="auto">
                            <source src="{{url('/voices')}}/{{$v->pron_3rd}}" type="video/mp4">
                        </video>
                        @endif
                    @endif
                    <h3 style="margin-bottom:20px"> <button type="button" urltype="{{ collect(request()->segments())->last() }}" wordid="{{$v->id}}" class="memorize btn btn-success btn-secondary btn-lg">Mark as memorized</button>
                    </h3>
                </div> 
                @endforeach

            </div>
            <a class="carousel-control-prev" href="#carouselContent" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>    


    </div>
    @else
    <div class="bs-callout bs-callout-danger" id="callout-inputgroup-text-input-only" style="border-left-color: #ce4844"><h4>There is nothing to display</h4> <p>Please change to other letter or display all results</p> </div>
    @endif
    {{ $words->links() }}
    <script type="text/javascript">$('.carousel').carousel({interval: false, keyboard: true})</script>
    @endsection
