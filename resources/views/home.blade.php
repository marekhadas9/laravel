@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        @include('navbarmenu')
    </div>
    <div class="box">
        <div id="calendar"></div>
    </div>
    <div class="box">
        <h2>Useful videos</h2>
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://img.youtube.com/vi/QTJ02h7uiXs/0.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Learn English in 3 Hours</h5>
                <p class="card-text">This 3 hours of English topics WILL make your English sound more natural!</p>
                <a href="https://www.youtube.com/embed/QTJ02h7uiXs" class="btn btn-primary">Watch</a>
            </div>
        </div>
    </div>
    <div class="box">
        <h2>Recently added</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recently_added as $word)
                    <tr>
                        <td>{{$word->id}}</td>
                        <td>{{$word->engname}}</td>
                        <td>{{$word->plname}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection
