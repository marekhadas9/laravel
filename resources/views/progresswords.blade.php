@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Your progress of learning</h1>
         @include('navbarmenu')
    </div>

    @if(!empty($totalLearnt))
    <div class="box">
        <h4>{{$page_type}} you have learnt</h4>

        @if($percent <= 25)
        <div class="progress" style="height:25px;margin-top:10px;margin-bottom:10px;font-weight:bold;text-align: center">
            <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100">{{$percent}}%</div>
        </div>
        @endif

        @if($percent >= 26 && $percent <= 49)
        <div class="progress" style="height:25px;margin-top:10px;margin-bottom:10px;font-weight:bold">
            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100">{{$percent}}% ({{$totalLearnt}}/{{$totalCnt}})</div>
        </div>
        @endif

        @if($percent >= 50 && $percent <= 74)
        <div class="progress" style="height:25px;margin-top:10px;margin-bottom:10px;font-weight:bold">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100">{{$percent}}% ({{$totalLearnt}}/{{$totalCnt}}))</div>
        </div>
        @endif

        @if($percent >= 75)
        <div class="progress" style="height:25px;margin-top:10px;margin-bottom:10px;font-weight:bold">
            <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100">{{$percent}}% ({{$totalLearnt}}/{{$totalCnt}})</div>
        </div>
        @endif

    
        <div class="table-responsive" style='margin-top:20px'>
            <table class="table table-striped table-sm align-middle">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                        <th>Learn Again</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $k => $v)
                    <tr class="wordslearnt wordidtb_{{$v->id}}">
                        <td class="align-middle">{{$v->id}}</td>
                        <td class="align-middle">
                            @if(empty($v->engname))
                            {{$v->eng_infinitive}}  - {{$v->past2nd}}  - {{$v->past3rd}} - {{$v->plname}}
                            @else
                            {{$v->engname}}
                            @endif
                        </td>
                         <td class="align-middle">{{$v->plname}}</td>
                        <td class="align-middle"><button class="btn btn-success learnagain" type="button" wordid="{{$v->id}}" urltype="{{ collect(request()->segments())->last() }}" class="btn btn-secondary btn-sm btn-success">Learn again</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
    @else
    <div class="bs-callout bs-callout-info" id="callout-pagination-label" style="border-left-color: #aa6708"> 
        <h4>You did not learnt anything so far.</h4> 
        <p>Please go to <a href="{{ route('words') }}"><b>"learn"</b></a> section to start learning new <b>{{strtolower($page_type)}}</b></p> 
    </div>
    @endif

    {{ $data->links() }}
    @endsection
