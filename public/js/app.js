

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
    var data = new Date();
    console.warn('Proficient English 2.0');

    $('#calendar').fullCalendar({
        viewRender: function (view, element) {
            app.getCallendar()
        }
    })




    $('.export').on('click', function () {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        var url = new URL(window.location.href);
        var page = url.searchParams.get("page") || '';
        var letter = url.searchParams.get("letter") || '';

        window.open(baseUrl + '/displaypdf?type=' + $(this).attr('page') + '&page=' + page + '&letter=' + letter);
    })

    $('.check').on('click', function () {
        var bd = $(this).parent().siblings().find('input');

        var inputVal = bd.val().toLowerCase();
        var properVal = bd.parent().attr('val').toLowerCase();

        if (inputVal == properVal) {
            bd.removeClass('inputsuccess inputfail').addClass('inputsuccess');
        } else {
            bd.removeClass('inputsuccess inputfail').addClass('inputfail');
        }
        ;
    })

    $('.hint').on("click", function () {
        $('.hintetext').text($(this).parent().siblings().find('input').parent().attr('val'));
        $('#modal').click();
    })

    $('.hint4').on("click", function () {

        var str = '';

        $(this).parent().siblings().find('input').each(function () {
            str += '<p><b>' + $(this).parent().attr('nm') + '</b> : <span style="color:#b24747"><b>' + $(this).parent().attr('val') + '</b></span></p>';
        });

        $('.hintetext').html(str);
        $('#modal').click();

    })


    $("#dialog").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#opener").on("click", function () {
        $("#dialog").dialog("open");
    });

    $("#dashboardsSuggest").autocomplete({
        source: app.getSuggestions,
        minLength: 2,
        select: function (event, ui) {
//            log("Selected: " + ui.item.value + " aka " + ui.item.id);
        }
    });

    $('.memorize').on('click', function () {
        var wordid = $(this).attr("wordid");
        var type = $(this).attr("urltype");
        app.memorize(wordid, type);
    })

    $('.learnagain').on('click', function () {
        var wordid = $(this).attr("wordid");
        var type = $(this).attr("urltype");
        app.learnAgain(wordid, type);
    })


})

var app = {
    applyCallendar: function (data) {

        $.each(data, function (date, obj) {
            var date = date;
            $.each(obj, function (type, val) {

                $('#calendar').fullCalendar('renderEvent', {
                    title: type + ' : ' + val,
                    start: date,
                    allDay: true
                });
            });
        });




    },
    getCallendar: function () {
        $.ajax({
            type: "POST",
            url: "/progress/progresscalendar",
            dataType: "json",
            success: function (resp) {
                app.applyCallendar(resp);
            }
        });
    },
    toggleFullScreen: function () {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    },
    getSuggestions: function (request, response) {

        $.ajax({
            type: "POST",
            url: "/word/suggestion",
            dataType: "json",
            data: {
                word: request.term,
                lang: 'en'
            },
            success: function (resp) {
                if (resp.status == 'success') {
                    response(resp.data);
                }

            }
        });
    },
    memorize: function (wordid, type) {
        $.ajax({
            type: "POST",
            url: "/learn/memorize",
            dataType: "json",
            data: {
                wordid: wordid,
                type: type,
                lang: 'en'
            },

            success: function (resp) {
                $('.carousel-control-next').trigger('click');
                setTimeout(function () {

                    if ($('.carousel-inner').children().size() <= 1) {
                        location.reload();
                    } else {
                        $('.word_' + wordid).fadeOut().remove();
                    }
                }, 1000);
            }
        });
    },
    learnAgain: function (wordid, type) {
        $.ajax({
            type: "POST",
            url: "/learn/learnagain",
            dataType: "json",
            data: {
                wordid: wordid,
                type: type,
                lang: 'en'
            },

            success: function (resp) {

                if ($('.wordslearnt').size() <= 1) {
                    location.reload();
                } else {
                    $('.wordidtb_' + wordid).fadeOut().remove();
                }

            }
        });
    }

}