<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/import', 'Import')->name('import');
Route::post('/import/textarea', 'Import@textarea')->name('importtextarea');
Route::post('/import/importpronunciation', 'Import@importpronunciation')->name('importpronunciation');

Route::post('/import/importtfile', 'Import@importtfile')->name('importtfile');

Route::get('/dictionary', 'Dictionary')->name('dictionary');

Route::post('/word/suggestion', 'Word@suggestion')->name('suggestion');
Route::post('/learn/memorize', 'Learn@memorize')->name('memorize');
Route::get('/learn/words', 'Learn@words')->name('words');
Route::get('/learn/phrasal', 'Learn@phrasal')->name('phrasal');
Route::get('/learn/idioms', 'Learn@idioms')->name('idioms');
Route::get('/learn/iregular', 'Learn@iregular')->name('iregular');

Route::post('/learn/learnagain', 'Learn@learnagain')->name('learnagain');


Route::get('/miscellaneous/tenses', 'Miscellaneous@tenses')->name('tenses');

Route::get('/progress/words', 'Progress@words')->name('wordprogress');
Route::get('/progress/phrasal', 'Progress@phrasal')->name('phrasalprogress');
Route::get('/progress/idioms', 'Progress@idioms')->name('idiomsprogress');
Route::get('/progress/iregular', 'Progress@iregular')->name('iregularprogress');


Route::get('/excercises/words', 'Excercises@words')->name('excword');
Route::get('/excercises/phrasal', 'Excercises@phrasal')->name('phrasalexc');
Route::get('/excercises/idioms', 'Excercises@idioms')->name('idiomsexc');
Route::get('/excercises/iregular', 'Excercises@iregular')->name('iregularexc');

Route::get('/learn/displaypdf', 'Learn@displaypdf')->name('displaypdf');

Route::post('/progress/progresscalendar', 'Progress@progresscalendar')->name('progresscalendar');
