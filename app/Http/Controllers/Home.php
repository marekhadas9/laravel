<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class Home extends Controller {

    public function __invoke() {
        if (Auth::check()) {
            return view('home');
        } else {
            return redirect()->route('login');
        }
    }

}
