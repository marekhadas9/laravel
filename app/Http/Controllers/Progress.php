<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use function GuzzleHttp\json_encode;
use function view;

class Progress extends Controller {

    public function progresscalendar() {
        $userId = Auth::id();
        
        $data = DB::table('memorized')
                    ->select('type','date_added')
                    ->where('userid', $userId)
                    ->get();

        
//        $resp = array(
//                    '2018-06-30' => array(
//                        'Prasal Verbs' => 2,
//                        'Words' => 33,
//                        'Irregular Verbs' => 234,
//                        'Idioms' => 223
//                    )
//                );
        
        $resp = array();
        foreach($data as $v){
            
            $da = date('Y-m-d',strtotime($v->date_added));
            $tp = $v->type;
            
            switch($tp){
                case 'words':
                    $type='Words';
                    break;
                case 'phrasal':
                    $type='Prasal Verbs';
                    break;
                case 'iregular':
                    $type='Irregular Verbs';
                    break;
                case 'idioms':
                    $type='Idioms';
                    break;
            }
            
            if(!isset($resp[$da])){
                $resp[$da] = array();
            }
            if(!isset($resp[$da][$type])){
                $resp[$da][$type] = 0;
            }
            $resp[$da][$type]+=1;
        };
       
        return json_encode($resp);
    }

    public function words() {
        $userId = Auth::id();

        $cntAll = DB::table('words')->count();

        $data = DB::table('words')
                ->select('engname', 'plname', 'id', 'pronunciation')
                ->where('active', 1)
                ->whereIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'words');
        });
        $cnt = $data->count();
        $data = $data->paginate(20);


        if (empty($cntAll)) {
            $percent = 0;
        } else {
            $percent = round((100 / $cntAll) * $cnt, 2);
        }

        return view('progresswords', ['page_type' => 'Words', 'totalLearnt' => $cnt, 'totalCnt' => $cntAll, 'percent' => $percent, 'data' => $data]);
    }

    public function phrasal() {
        $userId = Auth::id();

        $cntAll = DB::table('phrasals')->count();

        $data = DB::table('phrasals')
                ->select('engname', 'plname', 'id', 'example', 'pronunciation')
                ->where('active', 1)
                ->whereIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'phrasal');
        });
        $cnt = $data->count();
        $data = $data->paginate(20);


        if (empty($cntAll)) {
            $percent = 0;
        } else {
            $percent = round((100 / $cntAll) * $cnt, 2);
        }

        return view('progresswords', ['page_type' => 'Phrasals', 'totalLearnt' => $cnt, 'totalCnt' => $cntAll, 'percent' => $percent, 'data' => $data]);
    }

    public function idioms() {
        $userId = Auth::id();

        $cntAll = DB::table('idioms')->count();

        $data = DB::table('idioms')
                ->select('engname', 'plname', 'id', 'pronunciation')
                ->where('active', 1)
                ->whereIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'idioms');
        });
        $cnt = $data->count();
        $data = $data->paginate(20);

        if (empty($cntAll)) {
            $percent = 0;
        } else {
            $percent = round((100 / $cntAll) * $cnt, 2);
        }

        return view('progresswords', ['page_type' => 'Idioms', 'totalLearnt' => $cnt, 'totalCnt' => $cntAll, 'percent' => $percent, 'data' => $data]);
    }

    public function iregular() {
        $userId = Auth::id();

        $cntAll = DB::table('irregularverbs')->count();

        $data = DB::table('irregularverbs')
                ->select('id', 'eng_infinitive', 'past2nd', 'past3rd', 'plname')
                ->where('active', 1)
                ->whereIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'iregular');
        });
        $cnt = $data->count();
        $data = $data->paginate(20);


        if (empty($cntAll)) {
            $percent = 0;
        } else {
            $percent = round((100 / $cntAll) * $cnt, 2);
        }

        return view('progresswords', ['page_type' => 'Irregular verbs', 'totalLearnt' => $cnt, 'totalCnt' => $cntAll, 'percent' => $percent, 'data' => $data]);
    }

}

?>