<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function view;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __invoke() {
        if (Auth::check()) {
            return view('home');
        } else {
            return redirect()->route('login');
        }
    }
    
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index() {
        $ra = $this->getRecentlyAdded();
        return view('home',array('recently_added'=>$ra));
    }

    public function getRecentlyAdded() {
        $data = DB::table('words')
                ->select('engname', 'plname','id')
                ->where('active',1)
                ->orderByRaw('date_added DESC')
                ->limit(10)
                ->get();
        return $data;
    }

}
