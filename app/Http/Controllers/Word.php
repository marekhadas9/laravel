<?php

namespace App\Http\Controllers;

use App\EngWord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Word extends Controller {

    public function __invoke() {
        if (Auth::check()) {
            return view('home');
        } else {
            return redirect()->route('word');
        }
    }
    
    public function suggestion(Request $request) {

        $response = array(
            'status' => 'failed',
        );

        parse_str($request->getContent(), $data);

        if (!empty($data['word']) && !empty($data['lang'])) {
            $word = ucfirst($data['word']);
          
            $data = DB::table('words')
                    ->select('id', 'engname','plname as polname')
                    ->where('engname', 'LIKE', '%' . $word.'%')
                    ->limit(10)
                    ->get();


            $result = array();
            foreach ($data as $word) {
                $result[] = '(ENG) => '.$word->engname.' (POL) => '. $word->polname;
            }

            if (!empty($result)) {
                $response['status'] = 'success';
                $response['data'] = $result;
            }

           
            return json_encode($response);
        } else {
            return json_encode($response);
        }
    }


}
