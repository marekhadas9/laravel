<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use function redirect;
use function view;

class Dictionary extends Controller {

    public function __invoke() {
        if (!Auth::check()) {
            return view('home');
        }

        $ra = $this->getRecentlyAdded();
        return view('dictionary', array('dictionary' => $ra, 'letter' => $this->letter));
    }

    public function __construct(Request $req) {
        $this->letter = '';
        if ($req->input('letter')) {
            $this->letter = $req->input('letter');
        };
    }

    public function getRecentlyAdded() {
        $data = DB::table('words')
                ->select('engname', 'plname', 'id', 'pronunciation')
                ->where('active', 1);

        if ($this->letter) {
            $data = $data->where('engname', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);


        return $data;
    }

}
