<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Exception;
use Session;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\HttpFoundation\Request;
use function mb_strlen;
use function mb_strtolower;
use function mb_strtoupper;
use function mb_substr;
use function redirect;
use function view;

function va($p) {
    echo '<pre>';
    print_r($p);
    echo '</pre>';
}

class Import extends Controller {

    public function __invoke() {

        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $session = Session::get('status');

        $data = array();
        if (!empty($session)) {
            if (!empty($session['not_inserted'])) {
                $data['not_inserted'] = $session['not_inserted'];
            };
            if (!empty($session['inserted'])) {
                $data['inserted'] = $session['inserted'];
            };
        };

        return view('import', ['data' => $data]);
    }

    public function mb_ucfirst($string, $encoding) {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    public function importtfile(Request $req) {

        $file = $req->file('fileimpo');
        $content = file_get_contents($file->getRealPath());

        $type = $req->gridRadios;

        $result = $this->insertToDb($content, $type);

        $not_inserted = $result[1];
        $inserted = $result[0];

        return redirect('import')->with(
                        'status', array(
                    'not_inserted' => $not_inserted,
                    'inserted' => $inserted
                        )
        );
    }

    public function insertToDb($words, $type) {

        $inserted = array();
        $not_inserted = array();

        switch ($type) {
            case 1:
                $db_table = 'words';
                break;
            case 2:
                $db_table = 'idioms';
                break;
            case 3:
                $db_table = 'phrasals';
                break;
            case 4:
                $db_table = 'irregularverbs';
                break;
        }
        
        
        if (!empty($words)) {
            $words = explode("#", $words);
            $words = array_filter($words);


            foreach ($words as $w) {
                $wp = trim($w);

                $wm = explode("@", $wp);

                try {

                    switch ($type) {
                        case 1:
                            if (empty($wm[0]) || empty($wm[1])) {
                                throw new Exception('Not all values are specified', 1);
                            } else {
                                $db_insert = [
                                    'engname' => $this->mb_ucfirst(trim(mb_strtolower($wm[0])), 'utf-8'),
                                    'plname' => $this->mb_ucfirst(trim(mb_strtolower($wm[1])), 'utf-8')
                                ];
                            }
                            break;
                        case 2:
                            if (empty($wm[0]) || empty($wm[1]) || empty($wm[2])) {
                                throw new Exception('Not all values were specified', 2);
                            } else {
                                $db_insert = [
                                    'engname' => $this->mb_ucfirst(trim(mb_strtolower($wm[0])), 'utf-8'),
                                    'plname' => $this->mb_ucfirst(trim(mb_strtolower($wm[1])), 'utf-8'),
                                    'example' => $this->mb_ucfirst(trim(mb_strtolower($wm[2])), 'utf-8')
                                ];
                            }
                            break;
                        case 3:
                            if (empty($wm[0]) || empty($wm[1]) || empty($wm[2])) {
                                throw new Exception('Not all values were specified', 2);
                            } else {
                                $db_insert = [
                                    'engname' => $this->mb_ucfirst(trim(mb_strtolower($wm[0])), 'utf-8'),
                                    'plname' => $this->mb_ucfirst(trim(mb_strtolower($wm[1])), 'utf-8'),
                                    'example' => $this->mb_ucfirst(trim(mb_strtolower($wm[2])), 'utf-8')
                                ];
                            }
                            break;
                        case 4:
                            if (empty($wm[0]) || empty($wm[1]) || empty($wm[2]) || empty($wm[3])) {
                                throw new Exception('Not all values were specified', 3);
                            } else {
                                $db_insert = [
                                    'eng_infinitive' => $this->mb_ucfirst(trim(mb_strtolower($wm[0])), 'utf-8'),
                                    'past2nd' => $this->mb_ucfirst(trim(mb_strtolower($wm[1])), 'utf-8'),
                                    'past3rd' => $this->mb_ucfirst(trim(mb_strtolower($wm[2])), 'utf-8'),
                                    'plname' => $this->mb_ucfirst(trim(mb_strtolower($wm[3])), 'utf-8'),
                                ];
                            }
                            break;
                    }


                    $resp = DB::table($db_table)->insertGetId($db_insert);
                    $inserted[] = $db_insert;
                } catch (QueryException $e) {

                    $db_insert['reason'] = $e->getMessage();
                    $not_inserted[] = $db_insert;
                } catch (Exception $e) {

                    if ($e->getCode() == 1) {
                        $not_inserted[] = ['engname' => '-', 'plname' => '-', 'reason' => $e->getMessage()];
                    } elseif ($e->getCode() == 2) {
                        $not_inserted[] = ['engname' => '-', 'plname' => '-', 'example' => '-', 'reason' => $e->getMessage()];
                    } else {
                        $not_inserted[] = ['eng_infinitive' => '', 'past2nd' => '', 'past3rd' => '', 'plname' => '', 'reason' => $e->getMessage()];
                    }
                }
            }
        }
        return array($inserted, $not_inserted);
    }

    public function textarea(Request $req) {

        $words = $req->textareaimport;
        $type = $req->gridRadiosTextarea;

        $result = $this->insertToDb($words, $type);


        return redirect('import')->with(
                        'status', array(
                    'not_inserted' => $result[1],
                    'inserted' => $result[0]
                        )
        );
    }

    public function downloadPron($engname, $tablename, $field = 'pronunciation', $id) {
        
        $succes = array();
        $fail = array();
        
        $word = str_replace(' ', '%20', trim($engname));

        $filename = preg_replace('/\s+/', '_', trim(strtolower($engname))) . '.mp3';



        $voice_file = file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=' . $word . '&tl=En-gb');

        if($voice_file){
            $status = @file_put_contents('voices/' . $filename, $voice_file);
        }else{
            $status = false;
        }

        if ($voice_file && $status) {
            $succes [] = $engname;

            $data = DB::table($tablename)
                    ->where('id', $id)
                    ->update([$field => $filename]);
        } else {
            $fail [] = $engname;
        };

        return array(
            $succes, $fail
        );
    }

    public function importpronunciation() {

        set_time_limit(0);
        
        $words = DB::table('words')
                ->select('engname', 'plname', 'id')
                ->where('pronunciation', NULL)
                ->where('active', 1)
                ->get();

        $phrasals = DB::table('phrasals')
                ->select('engname', 'plname', 'id')
                ->where('pronunciation', NULL)
                ->where('active', 1)
                ->get();

        $idioms = DB::table('idioms')
                ->select('engname', 'plname', 'id')
                ->where('pronunciation', NULL)
                ->where('active', 1)
                ->get();

        $irregularverbs = DB::table('irregularverbs')
                ->select('eng_infinitive', 'past2nd', 'past3rd', 'plname', 'id')
                ->where('pron_infinitive', NULL)
                ->orWhere('pron_2nd', NULL)
                ->orWhere('pron_3rd', NULL)
                ->where('active', 1)
                ->get();


        $succes = array();
        $fail = array();

        foreach ($words as $p) {

            $res = $this->downloadPron($p->engname, 'words', 'pronunciation', $p->id);
            foreach ($res[0] as $r) {
                $succes[] = $r;
            }
            foreach ($res[1] as $r) {
                $fail[] = $r;
            }
        }

        foreach ($phrasals as $p) {

            $res = $this->downloadPron($p->engname, 'phrasals', 'pronunciation', $p->id);

            foreach ($res[0] as $r) {
                $succes[] = $r;
            }
            foreach ($res[1] as $r) {
                $fail[] = $r;
            }
        }

        foreach ($idioms as $p) {

            $res = $this->downloadPron($p->engname, 'idioms', 'pronunciation', $p->id);

            foreach ($res[0] as $r) {
                $succes[] = $r;
            }
            foreach ($res[1] as $r) {
                $fail[] = $r;
            }
        }

        foreach ($irregularverbs as $p) {

            if (empty($p->pron_infinitive)) {
                $res = $this->downloadPron($p->eng_infinitive, 'irregularverbs', 'pron_infinitive', $p->id);

                foreach ($res[0] as $r) {
                    $succes[] = $r;
                }

                foreach ($res[1] as $r) {
                    $fail[] = $r;
                }
            }

            if (empty($p->pron_2nd)) {
                $res2 = $this->downloadPron($p->past2nd, 'irregularverbs', 'pron_2nd', $p->id);
                foreach ($res2[0] as $r) {
                    $succes[] = $r;
                }

                foreach ($res2[1] as $r) {
                    $fail[] = $r;
                }
            }

            if (empty($p->pron_3rd)) {
                $res3 = $this->downloadPron($p->past3rd, 'irregularverbs', 'pron_3rd', $p->id);

                foreach ($res3[0] as $r) {
                    $succes[] = $r;
                }

                foreach ($res3[1] as $r) {
                    $fail[] = $r;
                }
            }
        }

        return view(
                'import', [
            'statusinsert' => array(
                'success' => $succes,
                'fail' => $fail
            )]
        );
    }

}
