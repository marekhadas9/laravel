<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use function view;

class Excercises extends Controller {

    public $letter;

    public function __construct(Request $req) {
        $this->letter = '';
        if ($req->input('letter')) {
            $this->letter = $req->input('letter');
        };
    }

    public static function word_replace($word){
        
       
        $word_length = strlen($word);
        
        if($word_length<4){
            $strip = 1;
        }elseif($word_length>=4 && $word_length <= 5){
            $strip = 2;
        }elseif($word_length>=6 && $word_length <= 7){
            $strip = 3;
        }elseif($word_length>=8 && $word_length <= 9){
            $strip = 4;
        }elseif($word_length>9 && $word_length <= 10){
            $strip = 5;
        }elseif($word_length>=11 && $word_length <= 13){
            $strip = 7;
        }elseif($word_length>13 ){
            $strip = 8;
        }
        
        $word = str_split($word);
        
        for($i=0;$i<=$strip;$i++){
            $ky = rand(0,$word_length-1);
            $word[$ky] = ' _ ';
        }
        
        return implode('',$word);    
      
    }
    
    public function words() {

        $userId = Auth::id();
        $data = DB::table('words')
                ->select('engname', 'plname', 'id', 'pronunciation')
                ->where('active', 1)
                ->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'words');
        });

        if ($this->letter) {
            $data->where('engname', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);


        return view('wordsexc', ['page_type' => 'Words - excercises', 'route' => 'words', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function phrasal() {
        $userId = Auth::id();

        $data = DB::table('phrasals')
                ->select('engname', 'plname', 'example', 'id')
                ->where('active', 1)
                ->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'phrasal');
        });
        ;

        if ($this->letter) {
            $data = $data->where('engname', 'LIKE', $this->letter . '%');
        };
        $data = $data->paginate(20);
        return view('wordsexc', ['page_type' => 'Phrasal Verbs - excercises', 'route' => 'phrasals', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function idioms() {
        $data = DB::table('idioms')
                ->select('engname', 'plname', 'example', 'id')
                ->where('active', 1);

        if ($this->letter) {
            $data = $data->where('engname', 'LIKE', $this->letter . '%');
        };
        $data = $data->paginate(20);
        return view('wordsexc', ['page_type' => 'Idioms - excercises', 'route' => 'idioms','words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function iregular() {
        $userId = Auth::id();
        $data = DB::table('irregularverbs')
                        ->select('id', 'eng_infinitive', 'past2nd', 'past3rd', 'plname')
                        ->where('active', 1)->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
                    from('memorized')->
                    where('userid', $userId)->
                    where('type', 'irregularverbs');
        });
        ;

        if ($this->letter) {
            $data = $data->where('eng_infinitive', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);


        return view('wordsexc', ['page_type' => 'Irregular Verbs - excercises', 'route' => 'irregular','irregular' => 'true', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

}
