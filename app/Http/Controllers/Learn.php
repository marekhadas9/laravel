<?php

namespace App\Http\Controllers;

use Dompdf\Dompdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use function GuzzleHttp\json_encode;
use function view;

class Learn extends Controller {

    public $letter;

    public function __construct(Request $req) {
        $this->letter = '';
        if ($req->input('letter')) {
            $this->letter = $req->input('letter');
        };
    }

    public function displaypdf(Request $r) {
        
        $type = $r->input('type');
        $letter = $r->input('letter');
        $page = $r->input('page');

        if (!empty($letter)) {
            $this->letter = $letter;
        }

        switch ($type) {
            case 'words':
                $data = $this->words('get');
                $pg = 'Words';
                break;
            case 'idioms':
                $data = $this->idioms('get');
                $pg = 'Idioms';
                break;
            case 'phrasal':
                $pg = 'Phrasal Verbs';
                $data = $this->phrasal('get');
                break;
            case 'iregular':
                $pg = 'Irregular Verbs';
                $data = $this->iregular('get');
                break;
        }

        $dompdf = new Dompdf();

        $tpl = '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <style>
                 *{ font-family: DejaVu Sans !important;}
                </style>
                <title>Export</title>
                <link href="http://laravel/css/bootstrap.css?ver=1530356386" rel="stylesheet" type="text/css">
            </head>
            <body>
            <h1>' . $pg . ' - export</h1>';

        if ($type == 'iregular') {
            $tpl .= '
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Present Tense</th>
                        <th>Simple Past</th>
                        <th>Past participate</th>
                        <th>Polish meaning</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($data as $k => $v) {
                $tpl .= "<tr>
                            <td>" . $v->id . "</td>
                            <td>" . $v->eng_infinitive . "</td>
                            <td>" . $v->past2nd . "</td>
                            <td>" . $v->past3rd . "</td>
                            <td>" . mb_convert_encoding($v->plname, 'HTML-ENTITIES', 'UTF-8') . "</td>
        </tr>";
            }
        } else {
            $tpl .= '
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>English meaning</th>
                        <th>Polish meaning</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($data as $k => $v) {
                // var_dump($v->plname);exit;
                $tpl .= "<tr>
                            <td>" . $v->id . "</td>
                            <td>" . $v->engname . "</td>
                            <td>" . mb_convert_encoding($v->plname, 'HTML-ENTITIES', 'UTF-8') . "</td>
        </tr>";
            }
        }
        $tpl .= '</tbody></table></body>
</html>';

        $dompdf->loadHtml($tpl);

        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

    public function words($get = false) {

        $userId = Auth::id();
        $data = DB::table('words')
                ->select('engname', 'plname', 'id', 'pronunciation')
                ->where('active', 1)
                ->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'words');
        });

        if ($this->letter) {
            $data->where('engname', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);

        if ($get) {
            return $data;
        }

        return view('words', ['page_type' => 'Words', 'route' => 'words', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function phrasal($get = false) {
        $userId = Auth::id();

        $data = DB::table('phrasals')
                ->select('engname', 'plname', 'example', 'id', 'pronunciation')
                ->where('active', 1)
                ->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'phrasal');
        });
        ;

        if ($this->letter) {
            $data = $data->where('engname', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);

        if ($get) {
            return $data;
        }

        return view('words', ['page_type' => 'Phrasal Verbs', 'route' => 'phrasals', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function idioms($get = false) {

        $userId = Auth::id();
        
        $data = DB::table('idioms')
                ->select('engname', 'plname', 'example', 'id', 'pronunciation')
                ->where('active', 1)->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
            from('memorized')->
            where('userid', $userId)->
            where('type', 'idioms');
        });

        if ($this->letter) {
            $data = $data->where('engname', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);

        if ($get) {
            return $data;
        }

        return view('words', ['page_type' => 'Idioms', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function iregular($get = false) {

        $userId = Auth::id();

        $data = DB::table('irregularverbs')
                        ->select('id', 'eng_infinitive', 'past2nd', 'past3rd', 'plname','pron_infinitive','pron_2nd','pron_3rd')
                        ->where('active', 1)->whereNotIn('id', function($query) use($userId) {
            $query->select('word_id')->
                    from('memorized')->
                    where('userid', $userId)->
                    where('type', 'irregularverbs');
        });

        if ($this->letter) {
            $data = $data->where('eng_infinitive', 'LIKE', $this->letter . '%');
        };

        $data = $data->paginate(20);

        if ($get) {
            return $data;
        }

        return view('words', ['page_type' => 'Irregular Verbs', 'irregular' => 'true', 'words' => $data->appends(['letter' => $this->letter]), 'letter' => $this->letter]);
    }

    public function learnagain(Request $req) {
        $userId = Auth::id();

        $wordid = $req->input('wordid');
        $lang = $req->input('lang');
        $type = $req->input('type');

        $result = DB::table('memorized')->
                        where('type', $type)->
                        where('userid', $userId)->
                        where('word_id', $wordid)->delete();

        return json_encode(array('success' => true));
    }

    public function memorize(Request $req) {

        $wordid = $req->input('wordid');
        $lang = $req->input('lang');
        $type = $req->input('type');

        $userId = Auth::id();


        $resp = DB::table('memorized')->insertGetId(
                [
                    'word_id' => $wordid,
                    'type' => $type,
                    'userid' => $userId
                ]
        );
        return json_encode(array('success' => true));
    }

}
